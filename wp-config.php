<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'cf_db');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'tocsxkqetcvwlxj8drqy7l6ohfdzdybsvcnombompknwiy0cnzn789zylx8gzykk');
define('SECURE_AUTH_KEY',  'gfb1quh5gl9ki7io6ga5eyavrjxaspkbtxhaohdzooh8tt99wwmjwscfnobfyfa1');
define('LOGGED_IN_KEY',    'joclsfrnrdbwuxrp2ohsqq20xa9glrlucr8hrf7rml9pysfg4pnqfdaelyr6dvj2');
define('NONCE_KEY',        'rdku78j6rat9qxkvhb4ib08p34xssei9bauuy3t9xtyevsnwszmk69ge244ts6vk');
define('AUTH_SALT',        'wcz7pkgoz2jhmwpzvjbdxwwxfwpgmioir5wbu4rzcdkt6ndq1zc1gsipozkzptlr');
define('SECURE_AUTH_SALT', 'lt2k8yhrpboa0qylbidf1sbjo3927l5w2rjilqehip83odmz8icycoygcwzypizp');
define('LOGGED_IN_SALT',   'ou5twnfgrfgaonurfu2gg0ncedqaqtf4firguxf8ivlmzompco5yixyizufvrtz4');
define('NONCE_SALT',       'faxc1abhiyx1a1zqplg7fsusxlaijoyirihqpz7mvbdwmrnqiwfsoeu4xhxg2ow8');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'cf_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
